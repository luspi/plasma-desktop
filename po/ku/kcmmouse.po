# translation of kcminput.po to Kurdish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Erdal Ronahi <erdal.ronahi@nospam.gmail.com>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-24 01:57+0000\n"
"PO-Revision-Date: 2008-03-02 16:20+0100\n"
"Last-Translator: Erdal Ronahi <erdal.ronahi@nospam.gmail.com>\n"
"Language-Team: Kurdish <ku@li.org>\n"
"Language: ku\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: KBabel 1.11.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:100
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:105
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:116
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:136
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:158
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:182
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:184
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:73
#, kde-format
msgid "Device:"
msgstr ""

#: kcm/libinput/main.qml:97 kcm/libinput/main_deviceless.qml:49
#, fuzzy, kde-format
#| msgid "&General"
msgid "General:"
msgstr "&Giştî"

#: kcm/libinput/main.qml:99
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:119
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:124 kcm/libinput/main_deviceless.qml:51
#, kde-format
msgid "Left handed mode"
msgstr ""

#: kcm/libinput/main.qml:144 kcm/libinput/main_deviceless.qml:71
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:76
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:169 kcm/libinput/main_deviceless.qml:96
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:178 kcm/libinput/main_deviceless.qml:105
#, kde-format
msgid "Pointer speed:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/libinput/main.qml:210 kcm/libinput/main_deviceless.qml:137
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr ""

#: kcm/libinput/main.qml:241 kcm/libinput/main_deviceless.qml:168
#, kde-format
msgid "None"
msgstr ""

#: kcm/libinput/main.qml:245 kcm/libinput/main_deviceless.qml:172
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:251 kcm/libinput/main_deviceless.qml:178
#, kde-format
msgid "Standard"
msgstr ""

#: kcm/libinput/main.qml:255 kcm/libinput/main_deviceless.qml:182
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:266 kcm/libinput/main_deviceless.qml:193
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/main.qml:268 kcm/libinput/main_deviceless.qml:195
#, kde-format
msgid "Invert scroll direction"
msgstr ""

#: kcm/libinput/main.qml:284 kcm/libinput/main_deviceless.qml:211
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:289
#, kde-format
msgid "Scrolling speed:"
msgstr ""

#: kcm/libinput/main.qml:337
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/main.qml:343
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/main.qml:354
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:390
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:420
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:421
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:425
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:442
#, kde-format
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr ""

#: kcm/libinput/main.qml:443
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:472
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "&Giştî"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, kde-format
msgid "Righ&t handed"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "Pêşkeftî"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr " msec"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, kde-format
msgid "Keyboard Navigation"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, kde-format
msgid "R&epeat interval:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, kde-format
msgid "Ma&ximum speed:"
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, kde-format
msgid " pixel/sec"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr ""

#: kcm/xlib/xlib_config.cpp:267 kcm/xlib/xlib_config.cpp:272
#, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] " pîksel"
msgstr[1] " pîksel"

#: kcm/xlib/xlib_config.cpp:277
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] " rêzik"
msgstr[1] " rêzik"

#~ msgid "Mouse"
#~ msgstr "Mişk"

#~ msgid "Patrick Dowler"
#~ msgstr "Patrick Dowler"

#~ msgid "Dirk A. Mueller"
#~ msgstr "Dirk A. Mueller"

#~ msgid "David Faure"
#~ msgstr "David Faure"

#~ msgid "Bernd Gehrmann"
#~ msgstr "Bernd Gehrmann"

#~ msgid "Rik Hemsley"
#~ msgstr "Rik Hemsley"

#~ msgid "Brad Hughes"
#~ msgstr "Brad Hughes"

#~ msgid "Brad Hards"
#~ msgstr "Brad Hards"

#~ msgid "Ralf Nolden"
#~ msgstr "Ralf Nolden"

#~ msgid "Name"
#~ msgstr "Naw"

#~ msgid "Description"
#~ msgstr "Rave"

#~ msgid "Small black"
#~ msgstr "Biçûk reş"

#~ msgid "Large black"
#~ msgstr "Mezin reş"

#~ msgid "Small white"
#~ msgstr "Biçûk sipî"

#~ msgid "Large white"
#~ msgstr "Mezin sipî"

#, fuzzy
#~| msgid " msec"
#~ msgctxt "milliseconds. time to automatically select the items"
#~ msgid " ms"
#~ msgstr " msec"

#, fuzzy
#~| msgid "none"
#~ msgctxt "no cordless mouse"
#~ msgid "none"
#~ msgstr "ti yek"

#~ msgid "Cordless Mouse"
#~ msgstr "Mişka Bêqablo"

#~ msgid "Cordless Mouse (2ch)"
#~ msgstr "Mişka Bêqablo (2kan)"

#~ msgid "Unknown mouse"
#~ msgstr "Mişka nenas"

#~ msgid "Battery Level"
#~ msgstr "Asta Pîlê"

#~ msgid "RF Channel"
#~ msgstr "Kanala RF"

#~ msgid "Channel 1"
#~ msgstr "Kanala 1"

#~ msgid "Channel 2"
#~ msgstr "Kanala 2"

#~ msgid "Confirmation"
#~ msgstr "Erêkirin"

#~ msgid "Overwrite Theme?"
#~ msgstr "Ser dirbî were nivîsîn?"

#~ msgid "KDE Classic"
#~ msgstr "KDE ya Klasîk"

#~ msgid "Short"
#~ msgstr "Kin"

#~ msgid "Long"
#~ msgstr "Dirêj"
