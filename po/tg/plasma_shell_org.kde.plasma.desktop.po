# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-26 02:00+0000\n"
"PO-Revision-Date: 2021-09-05 11:54-0700\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: Tajik Language Support\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.1\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Дар ҳоли истифода аст"

#: contents/activitymanager/ActivityItem.qml:245
#, fuzzy
#| msgid "Stop activity"
msgid ""
"Move to\n"
"this activity"
msgstr "Манъ кардан фаъолият"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Танзимот"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Манъ кардан фаъолият"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Фаъолиятҳои манъшуда:"

#: contents/activitymanager/ActivityManager.qml:120
#, fuzzy
#| msgid "Create activity..."
msgid "Create activity…"
msgstr "Эҷод кардани фаъолият..."

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Фаъолиятҳо"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "Танзимоти фаъолият"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "Нест кардан"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr ""

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr ""

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr ""

#: contents/applet/CompactApplet.qml:74
msgid "Open %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:20
#: contents/configuration/AppletConfiguration.qml:245
msgid "About"
msgstr ""

#: contents/configuration/AboutPlugin.qml:48
msgid "Send an email to %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:62
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:130
msgid "Copyright"
msgstr ""

#: contents/configuration/AboutPlugin.qml:148 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "Иҷозатнома:"

#: contents/configuration/AboutPlugin.qml:151
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr ""

#: contents/configuration/AboutPlugin.qml:165
#, fuzzy
#| msgid "Author:"
msgid "Authors"
msgstr "Муаллиф:"

#: contents/configuration/AboutPlugin.qml:176
msgid "Credits"
msgstr ""

#: contents/configuration/AboutPlugin.qml:188
msgid "Translators"
msgstr ""

#: contents/configuration/AboutPlugin.qml:205
msgid "Report a Bug…"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr "Миёнбурҳои клавиатура"

#: contents/configuration/AppletConfiguration.qml:293
msgid "Apply Settings"
msgstr "Танзимотро татбиқ кардан"

#: contents/configuration/AppletConfiguration.qml:294
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Танзимоти модули ҷорӣ тағйир ёфт. Шумо мехоҳед, ки тағйиротро татбиқ ё рад "
"кунед?"

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr "ХУБ"

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr "Татбиқ кардан"

#: contents/configuration/AppletConfiguration.qml:338
msgid "Cancel"
msgstr "Бекор кардан"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Тугмаи чап"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Тугмаи рост"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Тугмаи миёна"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Тугмаи-Ба қафо"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Тугмаи-Ба пеш"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Ҳаракат ба хати амудӣ"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Ҳаракат ба хати уфуқӣ"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:98
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:170
msgctxt "@title"
msgid "About"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:185
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Илова кардани амалиёт"

#: contents/configuration/ConfigurationContainmentAppearance.qml:67
msgid "Layout changes have been restricted by the system administrator"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:82
msgid "Layout:"
msgstr "Тарҳбандӣ:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:96
#, fuzzy
#| msgid "Wallpaper Type:"
msgid "Wallpaper type:"
msgstr "Навъи тасвири экран:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:116
msgid "Get New Plugins…"
msgstr "Гирифтани васлкунакҳои нав..."

#: contents/configuration/ConfigurationContainmentAppearance.qml:184
msgid "Layout changes must be applied before other changes can be made"
msgstr ""
"Пеш аз он ки тағйироти дигар тавонанд татбиқ карда шаванд, аввал тағйироти "
"тарҳбандӣ бояд татбиқ карда шаванд."

#: contents/configuration/ConfigurationContainmentAppearance.qml:188
#, fuzzy
#| msgid "Apply now"
msgid "Apply Now"
msgstr "Ҳозир татбиқ кардан"

#: contents/configuration/ConfigurationShortcuts.qml:17
#, fuzzy
#| msgid "Keyboard Shortcuts"
msgid "Shortcuts"
msgstr "Миёнбурҳои клавиатура"

#: contents/configuration/ConfigurationShortcuts.qml:29
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""
"Миёнбури зерин зербарномаро фаъол мекунад, гӯё ки шумо онро зер кардед."

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "Тасвири экран"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "Амалиётҳои муш"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Ба ин ҷо дарҷ кардан"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:43
#, fuzzy
#| msgid "Apply Settings"
msgid "Panel Settings"
msgstr "Танзимотро татбиқ кардан"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:49
#, fuzzy
#| msgid "Maximize Panel"
msgctxt "@action:button Make the panel as big as it can be"
msgid "Maximize"
msgstr "Лавҳаро ба ҳадди аксар сохтан"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:53
msgid "Make this panel as tall as possible"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:54
msgid "Make this panel as wide as possible"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:62
#, fuzzy
#| msgid "Delete"
msgctxt "@action:button Delete the panel"
msgid "Delete"
msgstr "Нест кардан"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:65
msgid "Remove this panel; this action is undo-able"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:89
#, fuzzy
#| msgid "Panel Alignment"
msgid "Alignment:"
msgstr "Москунии лавҳа"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Top"
msgstr "Боло"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Left"
msgstr "Чап"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the top; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the left; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:104
msgid "Center"
msgstr "Марказ"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:105
msgid ""
"Aligns a non-maximized panel to the center; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Bottom"
msgstr "Поён"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Right"
msgstr "Рост"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the bottom; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the right; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:142
#, fuzzy
#| msgid "Visibility"
msgid "Visibility:"
msgstr "Намоёнӣ"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:147
msgid "Always Visible"
msgstr "Ҳамеша намоён"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:155
#, fuzzy
#| msgid "Auto Hide"
msgid "Auto-Hide"
msgstr "Пинҳон кардан ба таври худкор"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:156
msgid ""
"Panel is hidden, but reveals itself when the cursor touches the panel's "
"screen edge"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:183
msgid "Opacity:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:188
msgid "Always Opaque"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:196
msgid "Adaptive"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:197
msgid ""
"Panel is opaque when any windows are touching it, and translucent at other "
"times"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:206
#, fuzzy
#| msgid "Always Visible"
msgid "Always Translucent"
msgstr "Ҳамеша намоён"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:233
msgid "Floating:"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:238
msgid "Floating"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:239
msgid "Panel visibly floats away from its screen edge"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:248
msgid "Attached"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:249
msgid "Panel is attached to its screen edge"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:271
#, fuzzy
#| msgid "Keyboard Shortcuts"
msgid "Focus Shortcut:"
msgstr "Миёнбурҳои клавиатура"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:281
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:65
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:26
#, fuzzy
#| msgid "Add Widgets..."
msgid "Add Widgets…"
msgstr "Илова кардани виҷетҳо..."

#: contents/configuration/panelconfiguration/ToolBar.qml:27
msgid "Add Spacer"
msgstr "Илова кардани ҷудокунанда"

#: contents/configuration/panelconfiguration/ToolBar.qml:28
#, fuzzy
#| msgid "More Settings..."
msgid "More Options…"
msgstr "Танзимоти бештар..."

#: contents/configuration/panelconfiguration/ToolBar.qml:226
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:265
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:286
msgid "Panel width:"
msgstr ""

#: contents/configuration/panelconfiguration/ToolBar.qml:286
#, fuzzy
#| msgid "Panel Alignment"
msgid "Panel height:"
msgstr "Москунии лавҳа"

#: contents/configuration/panelconfiguration/ToolBar.qml:406
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Пӯшидан"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
#, fuzzy
#| msgid "Remove Panel"
msgid "Remove Desktop"
msgstr "Тоза кардани лавҳа"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "Тоза кардани лавҳа"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr ""

#: contents/explorer/AppletAlternatives.qml:64
msgid "Alternative Widgets"
msgstr "Виҷетҳои иловагӣ"

#: contents/explorer/AppletDelegate.qml:167
msgid "Undo uninstall"
msgstr "Ботил сохтани лағви насб"

#: contents/explorer/AppletDelegate.qml:168
msgid "Uninstall widget"
msgstr "Лағв кардани насби виҷет"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "Муаллиф:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "Почтаи электронӣ:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "Лағв кардани насб"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
msgid "All Widgets"
msgstr "Ҳамаи виҷетҳо"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Виҷетҳо"

#: contents/explorer/WidgetExplorer.qml:152
msgid "Get New Widgets…"
msgstr "Гирифтани виҷетҳои нав..."

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "Намунаҳо"

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets matched the search terms"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets available"
msgstr ""

#~ msgid "Switch"
#~ msgstr "Гузариш"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Above"
#~ msgstr "Намоиши дар зери равзанаҳо"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Below"
#~ msgstr "Намоиши дар зери равзанаҳо"

#, fuzzy
#~| msgid "Windows Can Cover"
#~ msgid "Windows In Front"
#~ msgstr "Намоиши дар пеши равзанаҳо"

#~ msgid "Search…"
#~ msgstr "Ҷустуҷӯ..."

#~ msgid "Screen Edge"
#~ msgstr "Канори экран"

#~ msgid "Click and drag the button to a screen edge to move the panel there."
#~ msgstr ""
#~ "Барои ҳаракат кардани лавҳа ба канори дилхоҳи экран, ин тугмаро зер "
#~ "карда, ба канори дилхоҳ ҳаракат кунед."

#~ msgid "Width"
#~ msgstr "Паҳнӣ"

#~ msgid "Height"
#~ msgstr "Баландӣ"

#~ msgid "Click and drag the button to resize the panel."
#~ msgstr "Барои иваз кардани андозаи лавҳа тугмаро зер карда, кашед."

#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr "Тарҳбандӣ иваз карда намешавад, агар виҷетҳо қулф шуда бошанд"

#~ msgid "Lock Widgets"
#~ msgstr "Қулф кардани виҷетҳо"
